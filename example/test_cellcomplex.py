import numpy as np
import matplotlib.pyplot as plt

import importlib

import cellcomplex.cellcomplex
importlib.reload(cellcomplex.cellcomplex)
from cellcomplex import CellComplex

import cellcomplex.creation
importlib.reload(cellcomplex.creation)
from cellcomplex.creation import triangle_cellcomplex, poly_cellcomplex, dual_cellcomplex

import cellcomplex.analysis
importlib.reload(cellcomplex.analysis)
from cellcomplex.analysis import compute_cellcomplex_property

import cellcomplex.visualization
importlib.reload(cellcomplex.visualization)
from cellcomplex.visualization import plot_cellcomplex

import cellcomplex.example
importlib.reload(cellcomplex.example)
from cellcomplex.example import hexagonal_grid_cellcomplex, circle_voronoi_cellcomplex


# mesh = CellComplex(2)

# mesh.add_element(0)
# mesh.add_element(0)
# mesh.add_element(0)

# mesh.add_element(1)
# mesh.add_element(1)
# mesh.add_element(1)

# mesh.link(1,0,0)
# mesh.link(1,0,1)
# mesh.link(1,1,1)
# mesh.link(1,1,2)
# mesh.link(1,2,2)
# mesh.link(1,2,0)

# mesh.add_element(2)

# mesh.link(2,0,0)
# mesh.link(2,0,1)
# mesh.link(2,0,2)

# mesh.update_element_property('barycenter',0,[[0,0],[0,1],[1,0]])

# positions = dict(zip([1,2,3,4,5,6],[[0,0],[0,1],[1,0],[1,1],[2,1],[2,0]]))

# poly_mesh = poly_cellcomplex([[3,4,5,6]],positions)

# mesh = triangle_cellcomplex([[1,2,3],[2,3,4]],positions)

n_layers = 1

mesh = hexagonal_grid_cellcomplex(size=n_layers)
# perturbation = 0.5
# mesh[0]['barycenter'] = [p + (0.5-np.random.rand(3))*np.array([perturbation,perturbation,0]) for p in mesh[0]['barycenter'].values]

# mesh = circle_voronoi_cellcomplex(size=n_layers+1,circle_size=2*np.power(n_layers+1,2))

compute_cellcomplex_property(mesh,'vertices',2)
compute_cellcomplex_property(mesh,'oriented_vertices',2)
compute_cellcomplex_property(mesh,'barycenter',2)

compute_cellcomplex_property(mesh,'length',1)
compute_cellcomplex_property(mesh,'area',2)

compute_cellcomplex_property(mesh,'neighbors',0)
compute_cellcomplex_property(mesh,'edges',0)

figure = plt.figure(0)
figure.clf()
figure.patch.set_facecolor('w')

plot_cellcomplex(mesh,figure,dimension=0,color='b',size=40,element_ids=True)
# plot_cellcomplex(mesh,figure,dimension=1,color='r',alpha=0.5,element_ids=False)
plot_cellcomplex(mesh,figure,dimension=2,property_name='area',colormap='inferno',intensity_range=(0,4),coef=0.9,element_ids=False)
# plot_cellcomplex(mesh,figure,dimension=2,coef=0.9,element_ids=True)

# plot_cellcomplex(poly_mesh,figure,dimension=0,color='b',size=40)
# plot_cellcomplex(poly_mesh,figure,dimension=1,color='r',alpha=0.5)

# dual = dual_cellcomplex(mesh)
# plot_cellcomplex(dual,figure,dimension=1,color='k',alpha=0.5)
# plot_cellcomplex(dual,figure,dimension=2,alpha=0.5,coef=0.9)

figure.gca().axis('equal')

plt.show()