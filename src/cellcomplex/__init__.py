"""
Basic Cell Complex data structure and algorithms.
"""
# {# pkglts, base

from . import version

__version__ = version.__version__

# #}

from cellcomplex.cellcomplex import CellComplex
