# -*- coding: utf-8 -*-
# -*- python -*-
#
#       CellComplex
#
#       Copyright 2017-2018 Inria - INRA - CNRS
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
###############################################################################

"""
=========================
CellComplex
=========================

This module implements a data structure to represent cell complexes.

Functions
---------

.. autosummary::
   :toctree: generated/


"""

import numpy as np
import pandas as pd

from cellcomplex.utils.array_dict import array_dict

class CellComplex(object):
    """
    The data structure of the cellcomplex library.
    """

    def __init__(self, dimension=2):
        self._elements = [pd.DataFrame(columns=['borders','regions']) for i in range(dimension+1)]
    
    def __getitem__(self,key):
        if isinstance(key,tuple) and len(key) == 2:
            dimension = key[0]
            element_id = key[1]
            return self._elements[dimension].loc[element_id]
        elif isinstance(key,int):
            dimension = key
            return self._elements[dimension]
        else:
            raise KeyError(str(key))

    def __str__(self):
        topomesh_string = ""
        for d in range(self.dimension()+1):
            topomesh_string += "* Degree "+str(d)+"\n"
            topomesh_string += str(self._elements[d])
            if d<self.dimension():
                topomesh_string += "\n"
        return topomesh_string

    def dimension(self):
        return len(self._elements)-1

    def has_element (self, dimension, element_id) :
        if dimension == 0 :
            try :
                return element_id in self._elements[0].index
            except IndexError :
                raise StrInvalidDegree(dimension)
        else :
            try :
                return element_id in self._elements[dimension].index
            except IndexError :
                raise StrInvalidDegree(dimension)


    def _iter_borders (self, dimension, element_id) :
        return iter(self._elements[dimension].loc[element_id]['borders'])

    def _borders_with_offset (self, dimension, element_ids, offset) :
        if offset==0 :
            return element_ids
        else :
            ret = set()
            for element_id in element_ids :
                ret |= set(self._borders_with_offset(dimension-1,self._iter_borders(dimension,element_id),offset-1))
            return np.array(list(ret))

    def borders (self, dimension, element_id, offset = 1) :
        if dimension - offset < 0 :
            raise InvalidDegree ("smallest elements have no borders")
        return self._borders_with_offset(dimension,[element_id],offset)

    def nb_borders (self, dimension, element_id) :
        if dimension < 1 :
            raise InvalidDegree ("smallest elements have no borders")
        return len(self._elements[dimension].loc[element_id]['borders'])

    def _iter_regions (self, dimension, element_id) :
        """Internal function to access regions of an element
        """
        return iter(self._elements[dimension].loc[element_id]['regions'])
    
    def _regions_with_offset (self, dimension, element_ids, offset) :
        if offset == 0 :
            return element_ids
        else :
            ret = set()
            for element_id in element_ids :
                ret |= set(self._regions_with_offset(dimension + 1, self._iter_regions(dimension,element_id), offset - 1) )
            return np.array(list(ret))
    
    def regions (self, dimension, element_id, offset = 1) :
        if (dimension + offset) > self.dimension() :
            raise InvalidDegree ("biggest elements do not separate regions")
        return self._regions_with_offset(dimension,[element_id],offset)

    def nb_regions (self, dimension, element_id) :
        if dimension >= self.dimension() :
            raise InvalidDegree ("biggest elements do not separate regions")
        return len(self._elements[dimension].loc[element_id]['regions'])

    ########################################################################
    #
    #               Wisp list concept
    #
    ########################################################################
    def elements (self, dimension) :
        try :
            return self._elements[dimension].index
        except IndexError :
            raise StrInvalidDegree(dimension)

    def nb_elements (self, dimension) :
        try :
            return len(self._elements[dimension])
        except IndexError :
            raise StrInvalidDegree(dimension)

    ########################################################################
    #
    #               Neighborhood concept
    #
    ########################################################################
    def border_neighbors (self, dimension, element_id) :
        ret = set()
        for bid in self.borders(dimension,element_id) :
            for rid in self.regions(dimension-1,bid) :
                if rid != element_id :
                    ret |= set([rid])
        return np.array(list(ret)) 

    def nb_border_neighbors (self, dimension, element_id) :
        return len(self.border_neighbors(dimension,element_id))

    def region_neighbors (self, dimension, element_id) :
        ret = set()
        for rid in self.regions(dimension,element_id) :
            for bid in self.borders(dimension+1,rid) :
                if bid != element_id :
                    ret |= set([bid])
        return np.array(list(ret)) 

    def nb_region_neighbors (self, dimension, element_id) :
        return len(list(self.region_neighbors(dimension,element_id)))

    ########################################################################
    #
    #               Mutable mesh concept
    #
    ########################################################################
    def add_element (self, dimension, element_id = None) :
        if element_id is not None:
            if element_id in self._elements[dimension].index:
                raise TopomeshError("id "+str(element_id)+" already exists for dimension "+str(dimension)+"!")
        else:
            if len(self._elements[dimension])>0:
                element_id = self._elements[dimension].index.max()+1
            else:
                element_id = 0

        self._elements[dimension].loc[element_id] = None
        self._elements[dimension].loc[element_id][['borders','regions']] = [[],[]]
        return element_id

    def remove_element (self, dimension, element_id) :
        #remove links
        if dimension < self.dimension() :
            for rid in tuple(self.regions(dimension,element_id) ) :
                self.unlink(dimension+1,rid,element_id)
        if dimension > 0 :
            for bid in tuple(self.borders(dimension,element_id) ) :
                self.unlink(dimension,element_id,bid)
        self._elements[dimension].drop(element_id,inplace=True)

    def link (self, dimension, element_id, border_id) :
        if dimension < 1 :
            raise InvalidDegree("smallest elements have no borders")
        if not border_id in self._elements[dimension].loc[element_id]['borders']:
            self._elements[dimension].loc[element_id]['borders'] += [border_id]
            self._elements[dimension-1].loc[border_id]['regions'] += [element_id]

    def unlink (self, dimension, element_id, border_id) :
        if dimension < 1 :
            raise InvalidDegree("smallest elements have no borders")
        if border_id in self._elements[dimension].loc[element_id]['borders']:
            self._elements[dimension].loc[element_id]['borders'].remove(border_id)
        if (border_id in self._elements[dimension-1].index) and (element_id in self._elements[dimension-1].loc[border_id]['regions']):
            self._elements[dimension-1].loc[border_id]['regions'].remove(element_id)


    ########################################################################
    #
    #               Property mesh concept
    #
    ########################################################################
    def element_property_names(self,dimension):
        """todo"""
        return self._elements[dimension].columns

    def element_properties(self,dimension):
        """todo"""
        return self._elements[dimension]

    def element_property(self,property_name,dimension,element_ids = None):
        """todo"""
        try:
            if element_ids is not None:
                element_ids = np.array(element_ids)
                if element_ids.ndim == 1:
                    pandas_property = self._elements[dimension].loc[element_ids][property_name]
                else:
                    pandas_property = self._elements[dimension].loc[np.ravel(element_ids)][property_name].values.reshape(element_ids.shape)
            else:
                pandas_property = self._elements[dimension][property_name]
            return array_dict([np.array(v) for v in pandas_property.values],keys=pandas_property.index)
        except KeyError:
            raise PropertyError("property "+property_name+" is undefined on elements of dimension "+str(dimension))

    def add_element_property(self,property_name,dimension,values = None):
        """todo"""
        if property_name in self._elements[dimension].columns:
            raise PropertyError("property "+property_name+" is already defined on elements of dimension "+str(dimension))
        self._elements[dimension][property_name] = values

    def update_element_property(self,property_name,dimension,values,keys=None,erase_property=True):
        """todo"""
        if keys is None:
            if hasattr(values,"keys") and hasattr(values,"values"):
                keys = values.keys()
                if not property_name in self._elements[dimension].columns:
                    self._elements[dimension][property_name] = None
                self._elements[dimension][property_name].loc[keys] = list(values.values())
            else:
                self._elements[dimension][property_name] = values
        else:
            if not property_name in self._elements[dimension].columns:
                self._elements[dimension][property_name] = None
            self._elements[dimension][property_name].loc[keys] = values

    def has_element_property(self,property_name,dimension,is_computed=False):
        if property_name in self._elements[dimension].columns:
            return (not is_computed) or (not np.any(pd.isnull(self._elements[dimension][property_name])))
        else:
            return False
