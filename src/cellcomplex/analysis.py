import numpy as np
from scipy import ndimage as nd

from cellcomplex.utils.array_dict import array_dict

from copy import copy, deepcopy
from time import time

def compute_cellcomplex_property(cellcomplex, property_name, dimension=0, positions=None, verbose=True):

    if positions is None:
        positions = cellcomplex.element_property('barycenter',dimension=0)

    start_time = time()
    if verbose:
        print("--> Computing "+property_name+" property ("+str(dimension)+")")

    if property_name == 'barycenter':
        if not isinstance(positions,array_dict):
            positions = array_dict(positions)
        if not 'barycenter' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('barycenter',dimension=dimension)
        if dimension == 0:
            cellcomplex.update_element_property('barycenter',dimension=dimension,values=positions,keys=cellcomplex.elements(dimension))
        else:
            if not cellcomplex.has_element_property('vertices',dimension=dimension,is_computed=True):
                compute_cellcomplex_property(cellcomplex,'vertices',dimension=dimension)
            element_vertices = cellcomplex.element_property('vertices',dimension).values(cellcomplex.elements(dimension))
            element_vertices_number = np.array(list(map(len,element_vertices)))
            barycentrable_elements = cellcomplex.elements(dimension)[np.where(element_vertices_number>0)]
            element_vertices = cellcomplex.element_property('vertices',dimension).values(barycentrable_elements)
            vertices_positions = positions.values(element_vertices)
            if vertices_positions.dtype == np.dtype('O'):
                barycenter_positions = np.array([np.mean(p,axis=0) for p in vertices_positions])
            else:
                barycenter_positions = np.mean(vertices_positions,axis=1)
            cellcomplex.update_element_property('barycenter',dimension=dimension,values=list(barycenter_positions),keys=barycentrable_elements)

    # if property_name == 'barycenter':
    #     if not isinstance(positions,array_dict):
    #         positions = array_dict(positions)
    #     if not 'barycenter' in cellcomplex.element_property_names(dimension):
    #         cellcomplex.add_element_property('barycenter',dimension=dimension)
    #     if dimension == 0:
    #         cellcomplex.update_element_property('barycenter',dimension=dimension,values=positions,keys=np.array(list(cellcomplex.elements(dimension))))
    #     else:
    #         if not cellcomplex.has_element_property('vertices',dimension=dimension,is_computed=True):
    #             compute_cellcomplex_property(cellcomplex,'vertices',dimension=dimension)
    #         element_vertices = cellcomplex.element_property('vertices',dimension).values(np.array(list(cellcomplex.elements(dimension))))
    #         element_vertices_number = np.array(map(len,element_vertices))
    #         barycentrable_elements = np.array(list(cellcomplex.elements(dimension)))[np.where(element_vertices_number>0)]
    #         element_vertices = cellcomplex.element_property('vertices',dimension).values(barycentrable_elements)
    #         vertices_positions = positions.values(element_vertices)
    #         if vertices_positions.dtype == np.dtype('O'):
    #             barycenter_positions = np.array([np.mean(p,axis=0) for p in vertices_positions])
    #         else:
    #             barycenter_positions = np.mean(vertices_positions,axis=1)
    #         cellcomplex.update_element_property('barycenter',dimension=dimension,values=barycenter_positions,keys=barycentrable_elements)


    if property_name == 'vertices':
        if not 'vertices' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('vertices',dimension=dimension)
        if dimension == 0:
            cellcomplex.update_element_property('vertices',dimension=dimension,values=cellcomplex.elements(dimension).astype(int),keys=cellcomplex.elements(dimension))
        else:
            cellcomplex.update_element_property('vertices',dimension=dimension,values=[np.unique(cellcomplex.borders(dimension,e,dimension)).astype(int) for e in cellcomplex.elements(dimension)],keys=cellcomplex.elements(dimension))
        # if dimension == 1:
        #     cellcomplex.update_element_property('borders',dimension=dimension,values=[np.unique(cellcomplex.borders(dimension,e,dimension)).astype(int) for e in cellcomplex.elements(dimension)],keys=cellcomplex.elements(dimension))
    

    if property_name == 'edges':
        if not 'edges' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('edges',dimension=dimension)
        if dimension < 1:
            cellcomplex.update_element_property('edges',dimension=dimension,values=[cellcomplex.regions(dimension,w,1-dimension).astype(int) for w in cellcomplex.elements(dimension)],keys=cellcomplex.elements(dimension))
        elif dimension > 1:
            cellcomplex.update_element_property('edges',dimension=dimension,values=[cellcomplex.borders(dimension,w,dimension-1).astype(int) for w in cellcomplex.elements(dimension)],keys=cellcomplex.elements(dimension))
        else:
            cellcomplex.update_element_property('edges',dimension=dimension,values=cellcomplex.elements(dimension).astype(int),keys=cellcomplex.elements(dimension))

    # if property_name == 'triangles':
    #     if not 'triangles' in cellcomplex.element_property_names(dimension):
    #         cellcomplex.add_element_property('triangles',dimension=dimension)
    #     if dimension < 2:
    #         cellcomplex.update_element_property('triangles',dimension=dimension,values=np.array([np.array(list(cellcomplex.regions(dimension,w,2-dimension))).astype(int) for w in cellcomplex.elements(dimension)]),keys=np.array(list(cellcomplex.elements(dimension))))
    #     elif dimension > 2:
    #         cellcomplex.update_element_property('triangles',dimension=dimension,values=np.array([np.array(list(cellcomplex.borders(dimension,w,dimension-2))).astype(int) for w in cellcomplex.elements(dimension)]),keys=np.array(list(cellcomplex.elements(dimension))))
    #     else:
    #         cellcomplex.update_element_property('triangles',dimension=dimension,values=np.array(list(cellcomplex.elements(dimension))).astype(int),keys=np.array(list(cellcomplex.elements(dimension))))

    # if property_name == 'cells':
    #     if not 'cells' in cellcomplex.element_property_names(dimension):
    #         cellcomplex.add_element_property('cells',dimension=dimension)
    #     if dimension == cellcomplex.dimension():
    #         cellcomplex.update_element_property('cells',dimension=dimension,values=np.array(list(cellcomplex.elements(dimension))).astype(int),keys=np.array(list(cellcomplex.elements(dimension))))
    #     else:
    #         cellcomplex.update_element_property('cells',dimension=dimension,values=np.array([[int(c) for c in cellcomplex.regions(dimension,w,cellcomplex.dimension()-dimension)] for w in cellcomplex.elements(dimension)]),keys=np.array(list(cellcomplex.elements(dimension))))

    if property_name == 'oriented_borders':
        if dimension == 2:
            oriented_borders = []
            oriented_border_orientations = []
            for t,fid in enumerate(cellcomplex.elements(2)):
                face_eids = cellcomplex.borders(2,fid)
                #face_edges = np.array([list(cellcomplex.borders(1,eid)) for eid in face_eids])

                oriented_face_eids = [face_eids[0]]
                oriented_face_eid_orientations = [1]

                while len(oriented_face_eids) < len(face_eids):
                    current_eid = oriented_face_eids[-1]
                    current_eid_orientation = oriented_face_eid_orientations[-1]
                    if current_eid_orientation == 1:
                        start_pid, end_pid = cellcomplex.borders(1,current_eid)
                    else:
                        end_pid, start_pid = cellcomplex.borders(1,current_eid)
                    candidate_eids = set(list(cellcomplex.regions(0,end_pid))).intersection(set(list(face_eids))).difference({current_eid})
                    if len(oriented_face_eids)>0:
                        candidate_eids = candidate_eids.difference(set(oriented_face_eids))
                    if len(candidate_eids)>0:
                        next_eid = list(candidate_eids)[0]
                    else:
                        next_eid = list(set(list(face_eids)).difference(set(oriented_face_eids)))[0]
                    oriented_face_eids += [next_eid]
                    if end_pid == list(cellcomplex.borders(1,next_eid))[0]:
                        oriented_face_eid_orientations += [1]
                    else:
                        oriented_face_eid_orientations += [-1]
                # print oriented_face_eids," (",oriented_face_eid_orientations,")"
                oriented_borders += [oriented_face_eids]
                oriented_border_orientations += [oriented_face_eid_orientations]
                # raw_input()

            cellcomplex.update_element_property('oriented_borders',dimension=2,values=[b for b,o in zip(oriented_borders,oriented_border_orientations)],keys=cellcomplex.elements(2))
            cellcomplex.update_element_property('oriented_border_orientations',dimension=2,values=[o for b,o in zip(oriented_borders,oriented_border_orientations)],keys=cellcomplex.elements(2))


    if property_name == 'oriented_vertices':
        if dimension == 2:
            compute_cellcomplex_property(cellcomplex,'oriented_borders',2)
            oriented_vertices = []
            for t,fid in enumerate(cellcomplex.elements(2)):
                # oriented_face_eids = cellcomplex.element_property('oriented_borders',2)[fid][0]
                oriented_face_eids = cellcomplex.element_property('oriented_borders',2)[fid]
                # oriented_face_eid_orientations = cellcomplex.element_property('oriented_borders',2)[fid][1]
                oriented_face_eid_orientations = cellcomplex.element_property('oriented_border_orientations',2)[fid]
                oriented_face_pids = [list(cellcomplex.borders(1,eid))[0 if ori==1 else 1] for eid,ori in zip(oriented_face_eids,oriented_face_eid_orientations)]
                oriented_vertices += [oriented_face_pids]
            cellcomplex.update_element_property('oriented_vertices',dimension=2,values=oriented_vertices,keys=cellcomplex.elements(2))


    if property_name == 'neighbors':
        if not 'neighbors' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('neighbors',dimension=dimension)
        if dimension>0:
            cellcomplex.update_element_property('neighbors',dimension=dimension,values=[cellcomplex.border_neighbors(dimension,e).astype(int) for e in cellcomplex.elements(dimension)],keys=cellcomplex.elements(dimension))
        else:
            cellcomplex.update_element_property('neighbors',dimension=dimension,values=[cellcomplex.region_neighbors(dimension,e).astype(int) for e in cellcomplex.elements(dimension)],keys=cellcomplex.elements(dimension))

    # if property_name == 'valence':
    #     assert dimension<cellcomplex.dimension()
    #     if not 'valence' in cellcomplex.element_property_names(dimension):
    #         cellcomplex.add_element_property('valence',dimension=dimension)
    #     # if not cellcomplex.has_element_property('neighbors',dimension=dimension,is_computed=True):
    #     #     compute_cellcomplex_property(cellcomplex,'region_neighbors',dimension=dimension)
    #     # cellcomplex.update_element_property('valence',dimension=dimension,values=np.array([len(cellcomplex.element_property('neighbors',dimension)[w]) for w in cellcomplex.elements(dimension)]),keys=np.array(list(cellcomplex.elements(dimension))))
    #     cellcomplex.update_element_property('valence',dimension=dimension,values=np.array([len(list(cellcomplex.region_neighbors(dimension,w))) for w in cellcomplex.elements(dimension)]).astype(int),keys=np.array(list(cellcomplex.elements(dimension))))

    if property_name == 'length':
        assert dimension == 1
        if not 'length' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('length',dimension=dimension)
        vertices_positions = positions.values(cellcomplex.element_property('borders',dimension).values(cellcomplex.elements(dimension)))
        edge_vectors = vertices_positions[:,1] - vertices_positions[:,0]
        edge_lengths = np.power(np.sum(np.power(edge_vectors,2.0),axis=1),0.5)
        cellcomplex.update_element_property('length',dimension=dimension,values=list(edge_lengths),keys=cellcomplex.elements(dimension))

    if property_name == 'perimeter':
        assert dimension == 2
        if not 'perimeter' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('perimeter',dimension=dimension)
        if not cellcomplex.has_element_property('length',dimension=1,is_computed=True):
            compute_cellcomplex_property(cellcomplex,'length',dimension=1)
        edge_lengths = cellcomplex.element_property('length',dimension=1).values(cellcomplex.element_property('borders',dimension=dimension).values(cellcomplex.elements(dimension)))
        triangle_perimeters = list(map(np.sum,edge_lengths))
        cellcomplex.update_element_property('perimeter',dimension=dimension,values=triangle_perimeters,keys=cellcomplex.elements(dimension))

    if property_name == 'area':
        assert dimension == 2
        if not 'area' in cellcomplex.element_property_names(dimension):
            cellcomplex.add_element_property('area',dimension=dimension)
        if is_triangular(cellcomplex):
            if not cellcomplex.has_element_property('length',dimension=1,is_computed=True):
                compute_cellcomplex_property(cellcomplex,'length',dimension=1)
            edge_lengths = cellcomplex.element_property('length',dimension=1).values(cellcomplex.element_property('borders',dimension=dimension).values(cellcomplex.elements(dimension)))
            triangle_perimeters = np.sum(edge_lengths,axis=1)
            triangle_areas = np.sqrt(np.maximum(0,(triangle_perimeters/2.0)*(triangle_perimeters/2.0-edge_lengths[:,0])*(triangle_perimeters/2.0-edge_lengths[:,1])*(triangle_perimeters/2.0-edge_lengths[:,2])))
            cellcomplex.update_element_property('area',dimension=dimension,values=list(triangle_areas),keys=cellcomplex.elements(dimension))
        else:
            if not cellcomplex.has_element_property('oriented_vertices',dimension=2,is_computed=True):
                compute_cellcomplex_property(cellcomplex,'oriented_vertices',dimension=2)
            oriented_face_points = cellcomplex.element_property('barycenter',0).values(cellcomplex.element_property('oriented_vertices',2).values(cellcomplex.elements(2)))
            oriented_face_normals = [[0,0,1] for f in cellcomplex.elements(2)]
            face_areas = (1/2.)*np.abs(list(map(np.sum,[np.einsum("...ij,...ij->...i",np.array(n)[np.newaxis],np.cross(v[:-1],v[1:])) for v,n in  zip([list(p)+[p[0]] for p in oriented_face_points],oriented_face_normals)])))            
            cellcomplex.update_element_property('area',dimension=dimension,values=list(face_areas),keys=cellcomplex.elements(dimension))
       

def is_triangular(cellcomplex):
    if not 'vertices' in cellcomplex.element_property_names(2):
        compute_cellcomplex_property(cellcomplex,'vertices',dimension=2)
    face_vertices = cellcomplex.element_property('vertices',2).values()
    return (face_vertices.ndim == 2) and (face_vertices.shape[1] == 3)


