# -*- coding: utf-8 -*-
# -*- python -*-
#
#       CellComplex
#
#       Copyright 2017-2018 Inria - INRA - CNRS
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
###############################################################################

"""
=========================
CellComplex.Example
=========================

This module implements functions to generate some basic examples of CellComplex.

Functions
---------

.. autosummary::
   :toctree: generated/


"""

import numpy as np

from cellcomplex.creation import triangle_cellcomplex, poly_cellcomplex, dual_cellcomplex
from cellcomplex.analysis import compute_cellcomplex_property

from cellcomplex.utils.array_dict import array_dict
from cellcomplex.utils.delaunay_tools import delaunay_triangulation


def hexagonal_grid_cellcomplex(size=1, voxelsize=1., center=np.zeros(3)):
    """
    Generate a 2D regular hexagonal grid CellComplex

    This function creates a CellComplex with hexagonal faces (which are also
    the cells of the CellComplex) layed out on a regular 2D grid as successive
    layers.

    Parameters
    ----------
    size : int
        The number of hexagonal cell layers; 0 corresponds to a single hexagon,
        1 to a layer of 6 hexagons surrounding a central hexagon, and so on.

    voxelsize : float
        The length of the edges of the hexagons

    center : list or array of float
        The 3D position of the central point of the grid

    Returns
    -------
    cellcomplex : CellComplex
        A CellComplex instance representing the hexagonal grid
    """

    x = []
    y = []
    p = []
    for n in range(size+1,2*size+2):
        for r in [0,1]:
            p += list(len(p)+np.arange(n+r))
            x += list((np.arange(n+r)-((n+r-1)/2.))*voxelsize*np.sqrt(3.))
            row_y = (((2*size+1 - n)+1)//2)*3
            row_side = (((2*size+1 - n)+1)%2)
            y += list((row_y+(2*row_side-1)-(row_side==r)*(2*row_side-1)/2.)*np.ones(n+r)*voxelsize)


    for n in range(size+1,2*size+2)[::-1]:
        for r in [1,0]:
            p += list(len(p)+np.arange(n+r))
            x += list((np.arange(n+r)-((n+r-1)/2.))*voxelsize*np.sqrt(3.))
            row_y = (((2*size+1 - n)+1)//2)*3
            row_side = (((2*size+1 - n)+1)%2)
            y += list(-(row_y+(2*row_side-1)-(row_side==r)*(2*row_side-1)/2.)*np.ones(n+r)*voxelsize)

    x = np.array(x) + center[0]
    y = np.array(y) + center[1]
    z = np.zeros_like(x) + center[2]

    positions = dict(zip(range(len(x)),np.transpose([x,y,z])))

    index_list = []
    row_length_list = []
    row_gaps = []
    start = 0
    for n in range(size+1,2*size+1):
        index_list += list(np.arange(n)+start)
        start += n+n+1
        row_length_list += list(n*np.ones(n,int))
        for h in range(n):
            row_gaps += [[n+1,n+1,n+1,-(n+2),-(n+1)]]
    for n in [2*size+1]:
        index_list += list(np.arange(n)+start)
        start += n+n+2
        row_length_list += list(n*np.ones(n,int))
        for h in range(n):
            row_gaps += [[n+1,n+1,n,-(n+1),-(n+1)]]
    for n in range(2*size,size,-1):
        index_list += list(np.arange(n)+start)
        start += n+n+3
        row_length_list += list(n*np.ones(n,int))
        for h in range(n):
            row_gaps += [[n+2,n+1,n,-(n+1),-(n+1)]]

    # print index_list
    # print row_length_list

    # print "Hexagons"

    hexagons = []
    for i,n,gaps in zip(index_list,row_length_list,row_gaps):
        hexagon = [i]
        for p, gap in zip(range(5),gaps):
            hexagon += [hexagon[-1]+gap]
        hexagons += [hexagon]

    cellcomplex = poly_cellcomplex(hexagons,positions,faces_as_cells=True)

    return cellcomplex



def circle_voronoi_cellcomplex(size = 2,voxelsize = 1.,circle_size = 100.,z_coef = 0.):
    """
    Generate a 2D circular Voronoi diagram CellComplex
    """

    n_cells = 3*size*(size-1)+1
    radius = size*voxelsize

    circle_thetas = np.linspace(-np.pi,np.pi-2*np.pi/float(circle_size),circle_size)
    circle_points = np.transpose([radius*np.cos(circle_thetas),radius*np.sin(circle_thetas)])

    cell_thetas = np.array([np.pi*np.random.randint(-180,180)/180. for c in range(n_cells)])
    cell_distances = 0.5*radius*np.sqrt([np.random.rand() for c in range(n_cells)])

    cell_points = np.transpose([cell_distances*np.cos(cell_thetas),cell_distances*np.sin(cell_thetas)])

    omega_forces = dict(repulsion=0.5)
    sigma_deformation = 2.*radius/float(n_cells)

    for iteration in range(n_cells):
        cell_to_cell_vectors = np.array([[p-q for q in cell_points] for p in cell_points])
        cell_to_cell_distances = np.linalg.norm(cell_to_cell_vectors,axis=2)/radius
        cell_to_circle_vectors = np.array([[p-q for q in circle_points] for p in cell_points])
        cell_to_circle_distances = np.linalg.norm(cell_to_circle_vectors,axis=2)/radius
        
        deformation_force = np.zeros_like(cell_points)
        
        cell_repulsion_force = np.nansum(cell_to_cell_vectors/np.power(cell_to_cell_distances,3)[:,:,np.newaxis],axis=1)
        circle_repulsion_force = np.nansum(cell_to_circle_vectors/np.power(cell_to_circle_distances,3)[:,:,np.newaxis],axis=1)

        deformation_force += omega_forces['repulsion']*cell_repulsion_force
        deformation_force += 1.5*(n_cells/float(circle_size))*omega_forces['repulsion']*circle_repulsion_force

        deformation_force_amplitude = np.linalg.norm(deformation_force,axis=1)
        deformation_force = np.minimum(1.0,sigma_deformation/deformation_force_amplitude)[:,np.newaxis] * deformation_force

        cell_points += deformation_force
        cell_points = np.minimum(1.0,radius/(np.linalg.norm(cell_points,axis=1)))[:,np.newaxis] * cell_points

    all_positions = array_dict(np.transpose([np.concatenate([cell_points[:,0],circle_points[:,0]]),np.concatenate([cell_points[:,1],circle_points[:,1]]),np.zeros(n_cells+int(circle_size))]),keys=np.arange(n_cells+circle_size).astype(int))
    triangles = all_positions.keys()[delaunay_triangulation(all_positions.values())]

    radial_distances = np.linalg.norm(all_positions.values(),axis=1)
    radial_z = z_coef*np.power(radial_distances/radius,2)
    all_positions = array_dict(np.transpose([all_positions.values()[:,0],all_positions.values()[:,1],radial_z]),keys=all_positions.keys())

    triangulation_cellcomplex = triangle_cellcomplex(triangles,all_positions)
    cell_cellcomplex = dual_cellcomplex(triangulation_cellcomplex,2,vertex_positions='voronoi')

    # return triangulation_cellcomplex
    return cell_cellcomplex

