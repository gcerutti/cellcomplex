# -*- coding: utf-8 -*-
# -*- python -*-
#
#       CellComplex
#
#       Copyright 2017-2018 Inria - INRA - CNRS
#
#       File author(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       File contributor(s): Guillaume Cerutti <guillaume.cerutti@inria.fr>
#
#       Distributed under the Cecill-C License.
#       See accompanying file LICENSE or copy at
#           http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html
#
###############################################################################

"""
=========================
CellComplex.Creation
=========================

This module implements functions to easily generate cell complexes
from points, faces, or other cell complexes.

Functions
---------

.. autosummary::
   :toctree: generated/


"""

import numpy as np
from scipy.cluster.vq import vq

from cellcomplex import CellComplex
from cellcomplex.analysis import compute_cellcomplex_property, is_triangular

from cellcomplex.utils.array_dict import array_dict
from cellcomplex.utils.array_tools import array_unique
from cellcomplex.utils.geometry_tools import triangle_geometric_features

from copy import deepcopy

triangle_edge_list  = np.array([[1, 2],[0, 2],[0, 1]])


def triangle_cellcomplex(triangles, positions):
    """
    Generate a CellComplex from a list of trianges
    """

    triangles = np.array(triangles)
    positions = array_dict(positions)

    edges = array_unique(np.sort(np.concatenate(triangles[:,triangle_edge_list],axis=0)))

    triangle_edges = np.sort(np.concatenate(triangles[:,triangle_edge_list]))

    triangle_edge_matching = vq(triangle_edges,edges)[0]

    triangle_cellcomplex = CellComplex(3)
    for c in np.unique(triangles):
        triangle_cellcomplex.add_element(0,c)
    for e in edges:
        eid = triangle_cellcomplex.add_element(1)
        for pid in e:
            triangle_cellcomplex.link(1,eid,pid)
    for t in triangles:
        fid = triangle_cellcomplex.add_element(2)
        for eid in triangle_edge_matching[3*fid:3*fid+3]:
            triangle_cellcomplex.link(2,fid,eid)
    triangle_cellcomplex.add_element(3,0)
    for fid in triangle_cellcomplex.elements(2):
        triangle_cellcomplex.link(3,0,fid)
    triangle_cellcomplex.update_element_property('barycenter',0,list(positions.values(np.unique(triangles))),keys=np.unique(triangles))    

    return triangle_cellcomplex


def poly_cellcomplex(polys, positions, faces_as_cells=False):
    """
    Generate a CellComplex from a list of polygonal faces
    """

    polys = np.array(polys)
    positions = array_dict(positions)

    poly_lengths = np.array(list(map(len,polys)))
    poly_edge_list = [np.transpose([np.arange(l),(np.arange(l)+1)%l]) for l in poly_lengths]

    edges = array_unique(np.sort(np.concatenate([np.array(p)[l] for p,l in zip(polys,poly_edge_list)],axis=0)))

    poly_edges = np.sort(np.concatenate([np.array(p)[l] for p,l in zip(polys,poly_edge_list)],axis=0))

    poly_edge_matching = vq(poly_edges,edges)[0]

    poly_cellcomplex = CellComplex(3)
    for c in np.unique(np.concatenate(polys)):
        poly_cellcomplex.add_element(0,c)
    for e in edges:
        eid = poly_cellcomplex.add_element(1)
        for pid in e:
            poly_cellcomplex.link(1,eid,pid)
    total_poly_length = 0
    for q,l in zip(polys,poly_lengths):
        fid = poly_cellcomplex.add_element(2)
        for eid in poly_edge_matching[total_poly_length:total_poly_length+l]:
            poly_cellcomplex.link(2,fid,eid)
        total_poly_length += l
    if not faces_as_cells:
        poly_cellcomplex.add_element(3,0)
        for fid in poly_cellcomplex.elements(2):
            poly_cellcomplex.link(3,0,fid)
    else:
        for fid in poly_cellcomplex.elements(2):
            poly_cellcomplex.add_element(3,fid)
            poly_cellcomplex.link(3,fid,fid)
    poly_cellcomplex.update_element_property('barycenter',0,list(positions.values(np.unique(polys))),keys=np.unique(polys))    

    return poly_cellcomplex


def dual_cellcomplex(cellcomplex, dimension=2, vertex_positions='barycenter'):
    """
    Generate a CellComplex as the dual of a given CellComplex
    """

    dual_cellcomplex = CellComplex(cellcomplex.dimension())
    
    if dimension == 2:
        for d in range(3):
            if d<2:
                # dual_cellcomplex._regions[d] = deepcopy(cellcomplex._borders[2-d])
                dual_cellcomplex[d]['regions'] = deepcopy(cellcomplex[2-d]['borders'].values)
                dual_cellcomplex[d].index = deepcopy(cellcomplex[2-d].index.values)
            if d>0:
                dual_cellcomplex[d]['borders'] = deepcopy(cellcomplex[2-d]['regions'].values)
                dual_cellcomplex[d].index = deepcopy(cellcomplex[2-d].index.values)
        
        for e in dual_cellcomplex[2].index:
            dual_cellcomplex[3].loc[e] = None
        dual_cellcomplex[3]['borders'] = [[e] for e in dual_cellcomplex[2].index]
        dual_cellcomplex[2]['regions'] = [[e] for e in dual_cellcomplex[2].index]
        # dual_cellcomplex._borders[3] = dict(zip(dual_cellcomplex._borders[2].keys(),[[w] for w in dual_cellcomplex._borders[2].keys()]))
        # dual_cellcomplex._regions[2] = dict(zip(dual_cellcomplex._borders[2].keys(),[[w] for w in dual_cellcomplex._borders[2].keys()]))
    
        edges_to_remove = [e for e in dual_cellcomplex.elements(1) if len(dual_cellcomplex.borders(1,e))<2]
        faces_to_remove = [f for f in dual_cellcomplex.elements(2) if np.any([e in edges_to_remove for e in dual_cellcomplex.borders(2,f)])]
        cells_to_remove = faces_to_remove

    print(edges_to_remove)
    for e in edges_to_remove:
        dual_cellcomplex.remove_element(1,e)
    for f in faces_to_remove:
        dual_cellcomplex.remove_element(2,f)
    for c in cells_to_remove:
        dual_cellcomplex.remove_element(3,c)
    
    if 'voronoi' in vertex_positions:
        assert is_triangular(cellcomplex)
        if dimension==2:
            compute_cellcomplex_property(cellcomplex,'vertices',2)
            triangles = cellcomplex.element_property('vertices',2).values(list(dual_cellcomplex.elements(0)))
            positions = cellcomplex.element_property('barycenter',0)
            if vertex_positions == 'projected_voronoi':
                centers = triangle_geometric_features(triangles,positions)['projected_circumscribed_circle_center'].values
            else:
                centers = triangle_geometric_features(triangles,positions)['circumscribed_circle_center'].values
            dual_positions = array_dict(centers,list(dual_cellcomplex.elements(0)))
    else:
        if not cellcomplex.has_element_property('barycenter',dimension):
            compute_cellcomplex_property(cellcomplex,'barycenter',dimension)
        dual_positions = array_dict(cellcomplex.element_property('barycenter',dimension).values(list(dual_cellcomplex.elements(0))),list(dual_cellcomplex.elements(0)))
   
    dual_cellcomplex.update_element_property('barycenter',0,dual_positions)
    
    return dual_cellcomplex
