import numpy as np
from scipy.spatial.qhull import Delaunay, QhullError

from cellcomplex.utils.array_tools  import array_unique

tetra_triangle_edge_list  = np.array([[0,1],[0,2],[0,3],[1,2],[1,3],[2,3]])
tetra_triangle_list  = np.array([[0,1,2],[0,1,3],[0,2,3],[1,2,3]])
triangle_edge_list  = np.array([[1, 2],[0, 2],[0, 1]])

def delaunay_triangulation(points):
    if np.any(np.isnan(points)):
        triangles = np.array([])
    elif len(np.unique(points[:,2])) == 1:
        if np.all(points==0):
            triangles = np.array([])
        else:
            # try:
            triangles = Delaunay(np.array(points)[:,:2]).simplices
            # except QhullError:
            #    triangles = np.array([])
    else:
        if np.all(points==0):
            tetras = [[0,1,2,3]]
        else:
            # try:
            tetras = Delaunay(np.array(points)).simplices
            # except QhullError:
            #     tetras = [[0,1,2,3]]
        
        triangles = array_unique(np.sort(np.concatenate(tetras[:,tetra_triangle_list])))

    return triangles