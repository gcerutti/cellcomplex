import numpy as np
import pandas as pd

def triangle_geometric_features(triangles,positions):
    triangle_edge_list  = np.array([[1,2],[2,0],[0,1]])

    triangle_features = pd.DataFrame(columns=['area'])
    for t in range(len(triangles)):
        triangle_features.loc[t] = None

    triangle_edges = triangles[...,triangle_edge_list]
    triangle_edge_lengths = np.linalg.norm(positions.values(triangle_edges[...,1]) - positions.values(triangle_edges[...,0]),axis=2)

    triangle_features['edge_lengths'] = list(triangle_edge_lengths)

    triangle_features['barycenter'] = list(positions.values(triangles).mean(axis=1))

    triangle_features['perimeter'] = triangle_edge_lengths.sum(axis=1)
    triangle_features['area'] = np.sqrt(np.maximum(0,(triangle_features['perimeter']/2.0)*(triangle_features['perimeter']/2.0-triangle_edge_lengths[...,0])*(triangle_features['perimeter']/2.0-triangle_edge_lengths[...,1])*(triangle_features['perimeter']/2.0-triangle_edge_lengths[...,2])))


    triangle_features['eccentricity'] = 1. - (12.0*np.sqrt(3)*triangle_features['area'])/np.power(np.maximum(1e-5,triangle_features['perimeter']),2.0)
    
    # if ('max_distance' in features) or ('min_distance' in features):
    sorted_triangle_edge_lengths = np.sort(triangle_edge_lengths)
    triangle_features['max_distance'] = sorted_triangle_edge_lengths[:,-1]
    triangle_features['min_distance'] = sorted_triangle_edge_lengths[:,0]

    # if 'sinus' in features or 'sinus_eccentricity' in features or 'circumscribed_circle_center' in features or 'projected_circumscribed_circle_center' in features:
    sinuses = np.zeros_like(triangle_edge_lengths,np.float64)
    sinuses[:,0] = np.sqrt(np.maximum(0,np.array(1.0 - np.power(triangle_edge_lengths[...,1]**2+triangle_edge_lengths[...,2]**2-triangle_edge_lengths[:,0]**2,2.0)/np.power(np.maximum(1e-5,2.0*triangle_edge_lengths[...,1]*triangle_edge_lengths[...,2]),2.0),np.float64)))
    sinuses[:,1] = np.sqrt(np.maximum(0,np.array(1.0 - np.power(triangle_edge_lengths[...,2]**2+triangle_edge_lengths[...,0]**2-triangle_edge_lengths[:,1]**2,2.0)/np.power(np.maximum(1e-5,2.0*triangle_edge_lengths[...,2]*triangle_edge_lengths[...,0]),2.0),np.float64)))
    sinuses[:,2] = np.sqrt(np.maximum(0,np.array(1.0 - np.power(triangle_edge_lengths[...,0]**2+triangle_edge_lengths[...,1]**2-triangle_edge_lengths[:,2]**2,2.0)/np.power(np.maximum(1e-5,2.0*triangle_edge_lengths[...,0]*triangle_edge_lengths[...,1]),2.0),np.float64)))
    triangle_features['sinus'] = list(sinuses)
    # if 'cosinus' in features or 'circumscribed_circle_center' in features or 'projected_circumscribed_circle_center' in features:
    cosinuses = np.zeros_like(triangle_edge_lengths,np.float64)
    cosinuses[:,0] = (triangle_edge_lengths[...,1]**2+triangle_edge_lengths[...,2]**2-triangle_edge_lengths[:,0]**2)/np.power(np.maximum(1e-5,2.0*triangle_edge_lengths[...,1]*triangle_edge_lengths[...,2]),2.0)
    cosinuses[:,1] = (triangle_edge_lengths[...,2]**2+triangle_edge_lengths[...,0]**2-triangle_edge_lengths[:,1]**2)/np.power(np.maximum(1e-5,2.0*triangle_edge_lengths[...,2]*triangle_edge_lengths[...,0]),2.0)
    cosinuses[:,2] = (triangle_edge_lengths[...,0]**2+triangle_edge_lengths[...,1]**2-triangle_edge_lengths[:,2]**2)/np.power(np.maximum(1e-5,2.0*triangle_edge_lengths[...,0]*triangle_edge_lengths[...,1]),2.0)
    triangle_features['cosinus'] = list(cosinuses)

    triangle_features['angles'] = list(np.arccos(np.maximum(-1,np.minimum(1,cosinuses))))
    # print triangle_features['angles']*180./np.pi

    # if 'sinus_eccentricity' in features:
    triangle_features['sinus_eccentricity'] = 1.0 - (2.0*sinuses.sum(axis=1))/(3*np.sqrt(3))
    # print triangle_features['sinus_eccentricity']

    #if 'circumscribed_circle_center' in features or 'projected_circumscribed_circle_center' in features or 'orthocenter' in features:
    tangents = np.array(sinuses/(np.minimum(1,2*np.sign(cosinuses)+1)*np.maximum(1e-5,np.abs(cosinuses))),np.float64)
    triangle_features['tangent'] = list(tangents)
    triangle_features['orthocenter'] = list((positions.values(triangles)*tangents[:,:,np.newaxis]).sum(axis=1) / (np.minimum(1,2*np.sign(tangents.sum(axis=1))+1)*np.maximum(1e-5,np.abs(tangents.sum(axis=1))))[:,np.newaxis])

    # print triangle_features['tangent']
    # print (triangle_features['tangent'].sum(axis=1))

    #triangle_tangent_sums = triangle_features['tangent'][...,triangle_edge_list].sum(axis=1)/triangle_features['tangent'][...,triangle_edge_list].sum(axis=1)[:,np.newaxis]
    #triangle_tangent_sums = np.maximum(triangle_tangent_sums,-1)/np.maximum(triangle_tangent_sums,-1).sum(axis=1)[:,np.newaxis]
    triangle_sincos = sinuses*cosinuses
    # print triangle_sincos
    # print triangle_sincos.sum(axis=1)
    triangle_features['circumscribed_circle_center'] = list((positions.values(triangles)*triangle_sincos[:,:,np.newaxis]).sum(axis=1)/(triangle_sincos.sum(axis=1) + 1e-5)[:,np.newaxis])
    
    circ_center = np.array([np.array(c) for c in triangle_features['circumscribed_circle_center'].values])
    if triangle_sincos.min()<0:
        projected_triangle_edges = triangle_edges[np.where(triangle_sincos<0)[0]]
        projected_triangle_sincos = triangle_sincos[np.where(triangle_sincos<0)[0]]
        circ_center[np.where(triangle_sincos<0)[0]] = positions.values(projected_triangle_edges[projected_triangle_sincos<0]).mean(axis=1)
    triangle_features['projected_circumscribed_circle_center'] = list(circ_center)
    
    return triangle_features
