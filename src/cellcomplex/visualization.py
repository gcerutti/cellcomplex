import numpy as np

from cellcomplex.analysis import compute_cellcomplex_property, is_triangular

from matplotlib import cm
from matplotlib import tri
from matplotlib.colors import Normalize
from matplotlib.collections import PolyCollection
import matplotlib.pyplot as plt
# import matplotlib.patches as patch
# from matplotlib.collections import PatchCollection

# fig, ax = plt.subplots()
# patches = []
# num_polygons = 5
# num_sides = 5

# for i in range(num_polygons):
#     polygon = Polygon(np.random.rand(num_sides ,2), True)
#     patches.append(polygon)

# p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.4)


def plot_cellcomplex(cellcomplex,figure,dimension=2,coef=1,property_name="",property_dimension=None,colormap='viridis',color='k',alpha=1.0,cell_edges=False,intensity_range=None,linewidth=1,size=20,element_ids=False):

    if property_dimension is None:
        property_dimension = dimension

    positions = cellcomplex.element_property('barycenter',0)

    compute_cellcomplex_property(cellcomplex,'vertices',2)
    faces = cellcomplex.element_property('vertices',2).values(list(cellcomplex.elements(2)))
    
    compute_cellcomplex_property(cellcomplex,'barycenter',2)
    face_positions = np.concatenate([b + coef*(p-b) for p,b in zip(positions.values(faces),cellcomplex.element_property('barycenter',2).values())])
    if is_triangular(cellcomplex):
        face_triangles = np.arange(3*len(faces)).reshape((len(faces),3))
        triangulation = tri.Triangulation(face_positions[:,0],face_positions[:,1],face_triangles)

    if dimension==3:
        pass
    #     compute_cellcomplex_property(cellcomplex,'barycenter',3)
    #     compute_cellcomplex_property(cellcomplex,'triangles',3)
    #     cell_triangles = np.concatenate(cellcomplex.element_property('vertices',2).values(cellcomplex.element_property('triangles',3).values()))
    #     cell_triangle_cells = np.concatenate([c*np.ones_like(cellcomplex.element_property('triangles',3)[c]) for c in cellcomplex.elements(3)])
    #     cell_triangle_positions = np.concatenate([b + coef*(p-b) for p,b in zip(positions.values(cell_triangles),cellcomplex.element_property('barycenter',3).values(cell_triangle_cells))])
    #     cell_triangle_triangles = np.arange(3*len(cell_triangles)).reshape((len(cell_triangles),3))
    #     cell_triangulation = tri.Triangulation(cell_triangle_positions[:,0],cell_triangle_positions[:,1],cell_triangle_triangles)
    
    #     if (property_name is None) or (property_name == ""):
    #         cell_triangle_property = cell_triangle_cells
    #     else:
    #         if property_dimension == 3:
    #             cell_triangle_property = cellcomplex.element_property(property_name,property_dimension).values(cell_triangle_cells)
    #         elif property_dimension == 2:
    #             cell_triangle_property = cellcomplex.element_property(property_name,property_dimension).values(np.concatenate(cellcomplex.element_property('triangles',3).values()))
    #         elif property_dimension == 0:
    #             cell_triangle_property = cellcomplex.element_property(property_name,property_dimension).values(cell_triangles)
    #     if intensity_range is None:
    #         intensity_range = (cell_triangle_property.min(),cell_triangle_property.max())
    #     figure.gca().tripcolor(cell_triangulation,cell_triangle_property,cmap=colormap,alpha=alpha,vmin=intensity_range[0],vmax=intensity_range[1])
                

    elif dimension==2:

        if not is_triangular(cellcomplex):
            if not cellcomplex.has_element_property('oriented_vertices',dimension=2,is_computed=True):
                compute_cellcomplex_property(cellcomplex,'oriented_vertices',2)
            oriented_faces = cellcomplex.element_property('oriented_vertices',2).values(list(cellcomplex.elements(2)))
            oriented_face_positions = [b + coef*(p-b) for p,b in zip(positions.values(oriented_faces),cellcomplex.element_property('barycenter',2).values())]    

        if (property_name is None) or (property_name == ""):
            if cellcomplex.has_element_property('color',2):
                colors = cellcomplex.element_property('color',2).values()
            else:
                colors = np.zeros((len(faces),3))

            if is_triangular(cellcomplex):
                figure.gca().add_collection(PolyCollection(face_positions[:,:2].reshape((len(faces),3,2)),facecolors=colors,linewidth=0.5,alpha=0.8*alpha))
            else:
                figure.gca().add_collection(PolyCollection([p[:,:2] for p in oriented_face_positions],facecolors=colors,linewidth=0.5,alpha=0.8*alpha))
           
        else:
            if property_dimension == 2:
                face_property = cellcomplex.element_property(property_name,property_dimension).values()
            elif property_dimension == 0:
                face_property = np.concatenate(cellcomplex.element_property(property_name,property_dimension).values(faces))
            if face_property.ndim == 1:
                if intensity_range is None:
                    intensity_range = (face_property.min(),face_property.max())
                # mpl_colormap = cm.ScalarMappable(norm=Normalize(vmin=intensity_range[0], vmax=intensity_range[1]),cmap=cm.cmap_d[colormap])
                # colors = mpl_colormap.to_rgba(face_property)[:,:3]
                # print colors
                if is_triangular(cellcomplex):
                    if property_dimension == 2:
                        figure.gca().tripcolor(triangulation,face_property,cmap=colormap,alpha=alpha,vmin=intensity_range[0],vmax=intensity_range[1])
                    elif property_dimension == 0:
                        figure.gca().tripcolor(triangulation,face_property,cmap=colormap,alpha=alpha,vmin=intensity_range[0],vmax=intensity_range[1],shading='gouraud')
                else:
                    if property_dimension == 2:
                        mpl_colormap = cm.ScalarMappable(norm=Normalize(vmin=intensity_range[0], vmax=intensity_range[1]),cmap=cm.cmap_d[colormap])
                        colors = mpl_colormap.to_rgba(face_property)[:,:3]
                        figure.gca().add_collection(PolyCollection([p[:,:2] for p in oriented_face_positions],facecolors=colors,linewidth=0.5,alpha=alpha))

    elif dimension==1:
        if is_triangular(cellcomplex) and not cell_edges:
            figure.gca().triplot(triangulation,color=color,linewidth=linewidth,alpha=alpha,zorder=5)
        else:
            compute_cellcomplex_property(cellcomplex,'vertices',1)
            if cell_edges:
                boundary_edges = [e for e in cellcomplex.elements(1) if cellcomplex.nb_regions(1,e)==1]
                cell_boundary_edges = [e for e in cellcomplex.elements(1) if len(list(cellcomplex.regions(1,e,2)))>1]
                considered_edges = np.unique(cell_boundary_edges+boundary_edges)
            else:
                considered_edges = list(cellcomplex.elements(1))
            edge_points = cellcomplex.element_property('barycenter',0).values(cellcomplex.element_property('vertices',1).values(considered_edges))
            for p in edge_points:
                figure.gca().plot(p[:,0],p[:,1],color=color,linewidth=linewidth,alpha=alpha,zorder=5)


    elif dimension==0:
        if (property_name is None) or (property_name == ""):
            if cellcomplex.has_element_property('color',0):
                colors = cellcomplex.element_property('color',0).values()
            else:
                colors = color
            figure.gca().scatter(positions.values()[:,0],positions.values()[:,1],s=20,edgecolor=color,color=colors,alpha=alpha,zorder=6)
        else:
            vertex_property = cellcomplex.element_property(property_name,0).values(list(cellcomplex.elements(0)))
            if vertex_property.ndim == 1:
                if intensity_range is None:
                    intensity_range = (vertex_property.min(),vertex_property.max())
                figure.gca().scatter(positions.values(list(cellcomplex.elements(0)))[:,0],positions.values(list(cellcomplex.elements(0)))[:,1],c=vertex_property,s=size,linewidth=linewidth,cmap=colormap,alpha=alpha,vmin=intensity_range[0],vmax=intensity_range[1])

    if element_ids:
        if not cellcomplex.has_element_property('barycenter',dimension):
            compute_cellcomplex_property(cellcomplex,'barycenter',dimension)

        for e in cellcomplex.elements(dimension):
            figure.gca().text(cellcomplex.element_property('barycenter',dimension)[e][0],cellcomplex.element_property('barycenter',dimension)[e][1],str(e),color=color)

    # figure.canvas.draw()
    # plt.pause(1e-3)
