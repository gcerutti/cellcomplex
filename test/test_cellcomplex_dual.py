

import unittest

import numpy as np

from cellcomplex import CellComplex

from cellcomplex.analysis import compute_cellcomplex_property
from cellcomplex.example import hexagonal_grid_cellcomplex
from cellcomplex.creation import dual_cellcomplex



class TestCellComplexProperties(unittest.TestCase):

    def setUp(self):
        self.cellcomplex = hexagonal_grid_cellcomplex(size=1)
        self.dual = dual_cellcomplex(self.cellcomplex)

    def tearDown(self):
        pass


    def test_cellcomplex_dual_vertices(self):
        compute_cellcomplex_property(self.dual,'vertices',2)
        assert self.dual.element_property('vertices',2).values().shape[1] == 3

    def test_cellcomplex_dual_dual(self):
        dual_dual = dual_cellcomplex(self.dual,vertex_positions='voronoi')

        compute_cellcomplex_property(dual_dual,'vertices',2)
        assert dual_dual.element_property('vertices',2).values().shape[1] == 6