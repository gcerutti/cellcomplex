

import unittest

import numpy as np

from cellcomplex import CellComplex

from cellcomplex.analysis import compute_cellcomplex_property
from cellcomplex.example import hexagonal_grid_cellcomplex



class TestCellComplexProperties(unittest.TestCase):

    def setUp(self):
        self.cellcomplex = hexagonal_grid_cellcomplex(size=1)

    def tearDown(self):
        pass


    def test_cellcomplex_barycenter_property(self):
        compute_cellcomplex_property(self.cellcomplex,'barycenter',2)
        assert np.all(self.cellcomplex.element_property('barycenter',2)[3] == [0,0,0])

    def test_cellcomplex_vertices_property(self):
        compute_cellcomplex_property(self.cellcomplex,'vertices',2)
        assert self.cellcomplex.element_property('vertices',2).values().shape[1] == 6

    def test_cellcomplex_oriented_vertices_property(self):
        compute_cellcomplex_property(self.cellcomplex,'oriented_vertices',2)
        assert np.all(self.cellcomplex.element_property('oriented_vertices',2)[0] == [0,3,6,9,5,2]) or np.all(self.cellcomplex.element_property('oriented_vertices',2)[0] == [0,2,5,9,6,3])

    def test_cellcomplex_length_property(self):
        compute_cellcomplex_property(self.cellcomplex,'length',1)
        assert len(np.unique(self.cellcomplex.element_property('length',1).values()))==1

    def test_cellcomplex_area_property(self):
        compute_cellcomplex_property(self.cellcomplex,'area',2)
        assert len(np.unique(np.round(self.cellcomplex.element_property('area',2).values(),6)))==1