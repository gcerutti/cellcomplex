

import unittest

import numpy as np

from cellcomplex import CellComplex
from cellcomplex.creation import triangle_cellcomplex



class TestCellComplex(unittest.TestCase):

    def setUp(self):
        self.cellcomplex  = triangle_cellcomplex([[0, 1, 2]], {0:[0,0,0], 1:[0,1,0], 2:[1,1,0]})
        self.nbr_elements_cellcomplex = {dimension: len(list(self.cellcomplex.elements(dimension))) for dimension in range(4)}

    def tearDown(self):
        pass


    def test_cellcomplex_dimension(self):
         assert self.cellcomplex.dimension() == 3 

    def test_cellcomplex_nb_elements(self):
        assert self.cellcomplex.nb_elements(0) == 3
        assert self.cellcomplex.nb_elements(1) == 3
        assert self.cellcomplex.nb_elements(2) == 1
        assert self.cellcomplex.nb_elements(3) == 1

    def test_cellcomplex_elements(self):
        assert np.all(self.cellcomplex.elements(0) == [0,1,2])
        assert np.all(self.cellcomplex.elements(1) == [0,1,2])
        assert self.cellcomplex.elements(2) == [0]
        assert self.cellcomplex.elements(3) == [0]